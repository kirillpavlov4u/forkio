document.addEventListener("DOMContentLoaded", () => {
    const burger = document.getElementsByClassName('burger')[0];
    const burgerMenu = document.getElementsByClassName('burger-menu')[0];
    burger.addEventListener('click',  event => {
        burger.classList.toggle('burger-toggle');
        burgerMenu.classList.toggle('burger-menu-active')
    });
});