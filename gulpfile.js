const gulp = require("gulp"),
	browserSync = require("browser-sync"),
	autoprefixer = require("gulp-autoprefixer"),
	gulpClean = require("gulp-clean"),
	cleanCSS = require("gulp-clean-css"),
	concat = require("gulp-concat"),
	imageMin = require("gulp-imagemin"),
	jsMinify = require("gulp-js-minify"),
	sass = require("gulp-sass"),
	gulpUglify = require("gulp-uglify"),
	rename =  require("gulp-rename");

const paths = {
	src: {
		scripts: "src/script/**/*.js",
		scss: "src/scss/**/*.scss",
		img: "src/img/**/*.{png,gif,jpg,jpeg,svg}",
		html: "./index.html"
	},
	dist: {
		script: "dist/script",
		style: "dist/style",
		img: "dist/img",
		self: "./dist"
	}
};

const cleanDist = () => (
	gulp.src(paths.dist.self, {allowEmpty: true})
		.pipe(gulpClean())
);

const distCSS = () => (
	gulp.src(paths.src.scss)
		.pipe(sass().on('error', sass.logError))
		.pipe(cleanCSS())
		.pipe(autoprefixer({
			cascade: false
		}))
		.pipe(rename("styles.min.css"))
		.pipe(gulp.dest(paths.dist.style))
);

const distJS = () => (
	gulp.src(paths.src.scripts)
		.pipe(concat("scripts.min.js"))
		.pipe(jsMinify())
		.pipe(gulp.dest(paths.dist.script))
);

const distImages = () => (
	gulp.src(paths.src.img)
		.pipe(imageMin())
		.pipe(gulp.dest(paths.dist.img))
);

const changeWatcher = () => {
	browserSync.init({
		server: {
			baseDir: "./"
		}
	});

	gulp.watch(paths.src.scripts, distJS).on('change', browserSync.reload);
	gulp.watch(paths.src.scss, distCSS).on('change', browserSync.reload);
	gulp.watch(paths.src.img, distImages).on('change', browserSync.reload);
	gulp.watch(paths.src.html).on('change', browserSync.reload);
};

/*---------------Tasks---------------*/

gulp.task("cleanDist", cleanDist);

gulp.task("distCSS", distCSS);

gulp.task("distJS", distJS);

gulp.task("distImages", distImages);

gulp.task("build", gulp.series(
	"cleanDist",
	"distCSS",
	"distJS",
	"distImages"
));

gulp.task("dev", gulp.series(
	changeWatcher
));
