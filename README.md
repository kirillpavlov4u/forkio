# **Forkio** - *a web-page with responsive layout*
### K. Pavlov' & V. Nesternko' step-project.

> The web-page was delivered by using the following technological stack:
- JavaScript,
- SCSS,
- Gulp task-manager and following npm packages:
    - browser-sync,
    - gulp-autoprefixer,
    - gulp-clean,
    - gulp-clean-css,
    - gulp-concat,
    - gulp-imagemin,
    - gulp-js-minify,
    - gulp-sass,
    - gulp-rename;

> Team members: 
- Vladyslav Nesterenko (FE-17),
- Kyryl Pavlov (FE-17);

> Team members responsibilities:

- 1. V.Nesterenko:
    - Web-page sections:
        - Header and menu, 
        - `People Are Talking About Fork`;
    - Menu bar and its functionality implementation via JS;

- 2. K.Pavlov:
    - Web-page sections:
        - `Revolutionary Editor`, 
        - `Here is what you get`, 
        - `Fork Subscription Pricing`;
    - NPM packages management, 
    - GitLab repository management,
    - Web-page deployment via GitLab Pages,
    - basic project testing & debugging.